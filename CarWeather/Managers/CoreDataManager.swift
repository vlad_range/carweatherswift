//
//  CoreDataManager.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 23/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import Foundation
import MagicalRecord

class CoreDataManager {
  
  static func initContext() {
    MagicalRecord.setupCoreDataStack(withStoreNamed: "CarsAndWeather")
  }
  
  static func saveContext() {
    NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
  }
  
}
