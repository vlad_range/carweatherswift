//
//  CarManager.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 23/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import Foundation
import MagicalRecord

final class CarManager {
  // MARK: - Properties
  
  static var cars: [Car] {
    return Car.mr_findAllSorted(by: "timeStamp", ascending: false) as? [Car] ?? []
  }
  
  // MARK: - Actions
  
  static func addCar(name: String, price: String, engine: String, transmission: String, condition: String, carDescription: String, imagePaths: [String]) {
    let car = Car.mr_createEntity()
    car?.name = name
    car?.price = NSDecimalNumber(string: price)
    car?.engine = engine
    car?.transmission = transmission
    car?.condition = condition
    car?.carDescription = carDescription
    car?.imagePaths = imagePaths
    car?.timeStamp = Date()
    CoreDataManager.saveContext()
  }
  
  static func remove(car: Car) {
    car.mr_deleteEntity()
    CoreDataManager.saveContext()
  }
  
}
