//
//  WeatherManager.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 23/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import CoreLocation
import SwiftOpenWeatherMapAPI
import SwiftyJSON
import MagicalRecord

class WeatherManager {
  
  // MARK: - Properties
  
  static let weatherAPI = WAPIManager(apiKey: "3ba774115f83d18c54bef1b4165536c1", temperatureFormat: .Celsius, lang: .English)
  
  static var weather: Weather? {
    return Weather.mr_findFirst()
  }
  
  // MARK: - Actions
  
  static func getWeatherByCoordinates(coordinates: CLLocationCoordinate2D, completion: @escaping (_ weather: Weather) -> Void){
    weatherAPI.currentWeatherByCoordinatesAsJson(coordinates: coordinates) { (result) in
      if case .Success(let json) = result {
        completion(WeatherManager.weather(from: json))
      }
    }
  }
  
  static func weather(from json: JSON) -> Weather {
    var weather = WeatherManager.weather
    
    if weather == nil {
      weather = Weather.mr_createEntity()
    }
    
    weather?.cityName = json["name"].string
    weather?.weatherDescription = json["weather"].arrayValue.first!["main"].string
    weather?.iconPath = "http://openweathermap.org/img/w/\(json["weather"].arrayValue.first!["icon"].stringValue).png"
    weather?.temperature = Int16(json["main"]["temp"].intValue)
    CoreDataManager.saveContext()
    
    return weather!
  }
  
}
