//
//  ViewController.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 22/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit
import CoreLocation

final class CarListViewController: UIViewController {
  
  // MARK: - Constants
  
  static let carCellIdentifier = "carCell"
  
  static let carDetailsSegueIdentifier = "carDetailsSegue"
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var weatherView: WeatherView!
  
  // MARK: - Properties
  
  fileprivate var dataSource: [Car] = []
  
  fileprivate lazy var locationManager: CLLocationManager = {
    var locationManager = CLLocationManager()
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    return locationManager
  }()
  
  // MARK: - UIViewController life cycle

  override func viewDidLoad() {
    super.viewDidLoad()
    locationManager.delegate = self
    locationManager.requestWhenInUseAuthorization()
    
    tableView.tableFooterView = UIView()
    tableView.tableHeaderView = UIView()
    
    configureNavigationBar()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    dataSource = CarManager.cars
    tableView.reloadData()
    weatherView.weather = WeatherManager.weather
    locationManager.startUpdatingLocation()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    locationManager.stopUpdatingLocation()
  }
  
  // MARK: - Actions
  
  func configureNavigationBar() {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Back", tableName: "Main", comment: ""), style: .plain, target: nil, action: nil)
    navigationController?.navigationBar.setBottomBorderColor(color: .white, height: 1)
    navigationController?.navigationBar.shadowImage = UIImage()
    navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
  }
  
  // MARK: - Navigation
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == CarListViewController.carDetailsSegueIdentifier,
      let car = sender as? Car,
      let destination = segue.destination as? CarDetailsViewController {
      destination.car = car
    }
      
  }
  
}

extension CarListViewController: UITableViewDataSource, UITableViewDelegate {
  // MARK: - UITableView DataSource and Delegate methods
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if let cell = tableView.dequeueReusableCell(withIdentifier: CarListViewController.carCellIdentifier, for: indexPath) as? CarTableViewCell {
      cell.car = dataSource[indexPath.row]
      return cell
    }
    return UITableViewCell()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataSource.count
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    performSegue(withIdentifier: CarListViewController.carDetailsSegueIdentifier, sender: dataSource[indexPath.row])
  }
  
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    let action = UITableViewRowAction(style: .destructive, title: NSLocalizedString("DELETE", tableName: "Main", comment: "")) { (action, indexPath) in
      print(action)
      CarManager.remove(car: self.dataSource[indexPath.row])
      self.dataSource.remove(at: indexPath.row)
      tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    return [action]
  }
  
}

extension CarListViewController: CLLocationManagerDelegate {
  // MARK: - Core location manager delegate methods
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if let coordinate = locations.first?.coordinate {
      WeatherManager.getWeatherByCoordinates(coordinates: coordinate, completion: { [weak self] (weather) in
        self?.weatherView.weather = weather
      })
      locationManager.stopUpdatingLocation()
    }
    
  }
}
