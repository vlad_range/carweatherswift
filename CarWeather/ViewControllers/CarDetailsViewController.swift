//
//  CarDetailsViewController.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 25/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

final class CarDetailsViewController: UIViewController {
  // MARK: - IBOutlets
  
  @IBOutlet weak var carNameLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var engineLabel: UILabel!
  @IBOutlet weak var transmissionLabel: UILabel!
  @IBOutlet weak var conditionLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var photosView: PhotosView!
  
  // MARK: - Properties
  
  public var car: Car?
  
  // MARK: - UIViewController life cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
  }
  
  // MARK: - Actions
  
  func setupViews() {
    carNameLabel.text = car?.name
    priceLabel.text =  "\(car?.price ?? 0)$"
    engineLabel.text = car?.engine
    transmissionLabel.text = car?.transmission
    conditionLabel.text = car?.condition
    descriptionLabel.text = car?.carDescription
    photosView.imagePaths = car?.imagePaths ?? []
  }
  
}
