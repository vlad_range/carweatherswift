//
//  AddCarViewController.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 23/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//
import UIKit

final class AddCarViewController: UIViewController {
  // MARK: - Constants
  
  fileprivate struct PickerDataSource {
    static let engine = ["1.0V3", "1.4V4", "1.6V4", "1.8V6", "2.0V6", "5.0V12"]
    static let transmission = ["automatic", "manual"]
    static let condition = ["new", "like new", "good", "fair", "bad", "scrap"]
  }
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var carNameTextField: UITextField!
  @IBOutlet weak var priceTextField: UITextField!
  @IBOutlet weak var engineTextField: UITextField!
  @IBOutlet weak var transmissionTextField: UITextField!
  @IBOutlet weak var conditionTextField: UITextField!
  @IBOutlet weak var descriptionTextView: UITextView!
  
  @IBOutlet weak var pickerView: UIPickerView!
  @IBOutlet weak var pickerToolbar: UIToolbar!
  
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var addPhotoView: AddPhotoView!
  
  // MARK: - Properties
  
  fileprivate var pickerDataSource: [String] {
    if let currentFirstResponder = view.currentFirstResponder() {
      switch currentFirstResponder {
      case engineTextField:
        return PickerDataSource.engine
      case transmissionTextField:
        return PickerDataSource.transmission
      case conditionTextField:
        return PickerDataSource.condition
      default:
        return []
      }
    }
    return []
  }
  
  fileprivate lazy var picker: ImagePickerController = {
    let picker = ImagePickerController()
    picker.delegate = self
    picker.actionSheetTintColor = #colorLiteral(red: 0.5294117647, green: 0.8980392157, blue: 0.2823529412, alpha: 1)
    return picker
  }()
  
  fileprivate var isCarInformationFull: Bool {
    return carNameTextField.hasText &&
      priceTextField.hasText &&
      engineTextField.hasText &&
      transmissionTextField.hasText &&
      conditionTextField.hasText &&
      descriptionTextView.hasText &&
      addPhotoView.photoPaths.count > 0
  }
  
  // MARK: - UIViewController life cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    engineTextField.inputView = pickerView
    transmissionTextField.inputView = pickerView
    conditionTextField.inputView = pickerView
    engineTextField.inputAccessoryView = pickerToolbar
    transmissionTextField.inputAccessoryView = pickerToolbar
    conditionTextField.inputAccessoryView = pickerToolbar
    
    scrollView.setupKeyboardObserving()
    
    addPhotoView.delegate = self
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    scrollView.removeKeyboardObservers()
  }
  
  // MARK: - Actions
  
  fileprivate func saveCar() {
    CarManager.addCar(name: carNameTextField.text!,
                      price: String(priceTextField.text!.dropLast()),
                      engine: engineTextField.text!,
                      transmission: transmissionTextField.text!,
                      condition: conditionTextField.text!,
                      carDescription: descriptionTextView.text!,
                      imagePaths: addPhotoView.photoPaths)
    navigationController?.popViewController(animated: true)
  }
  
  // MARK: - IBActions
  
  @IBAction func okButtonTapped(_ sender: UIBarButtonItem) {
    if let firstResponder = view.currentFirstResponder() {
      switch firstResponder {
      case engineTextField:
        transmissionTextField.becomeFirstResponder()
      case transmissionTextField:
        conditionTextField.becomeFirstResponder()
      case conditionTextField:
        descriptionTextView.becomeFirstResponder()
      default:
        firstResponder.resignFirstResponder()
      }
    }
  }
  
  @IBAction func addCarTapped(_ sender: UIButton) {
    if isCarInformationFull {
      saveCar()
    } else {
      showAlert(message: "Please fill all required fields")
    }
  }
  
}

extension AddCarViewController: AddPhotoViewDelegate {
  // MARK: - AddPhotoView delegate methods
  
  func photosViewDidTappedAddPhotos() {
    picker.presentActionSheet(for: self, imageSourceOptions: [.camera, .photoLibrary])
  }
  
}

extension AddCarViewController: ImagePickerControllerDelegate {
  // MARK: - ImagePickerController delegate methods
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    guard let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
      return
    }
    
    defer {
      picker.dismiss(animated: true)
    }
    
    let imageName = (info[UIImagePickerControllerImageURL] as? URL)?.lastPathComponent ??
      "CameraPhoto_" + Date().description.replacingOccurrences(of: " ", with: "_") + ".png"
    
    let image = originalImage.fixOrientation()
    
    if let imagePath = FileManager.saveToDisk(image: image, with: imageName) {
      print("Saved image path: \(imagePath)")
      addPhotoView.photoPaths.append(imageName)
    }
  }
  
}

extension AddCarViewController: UITextFieldDelegate {
  //MARK: - UITextFiedl delegate methods
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    if textField.inputView == pickerView {
      pickerView.reloadComponent(0)
      let row = pickerDataSource.index(of: textField.text!) ?? 0
      pickerView.selectRow(row, inComponent: 0, animated: false)
      pickerView(pickerView, didSelectRow: row, inComponent: 0)
    }
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    switch textField {
    case engineTextField, transmissionTextField, conditionTextField:
      return false
    case priceTextField:
      let allowedCharacters = CharacterSet.decimalDigits
      
      if string.count == 0 && textField.text!.count > 1 {
        textField.text!.remove(at: textField.text!.index(textField.text!.endIndex, offsetBy: -2))
        return false
      }
      
      if string.rangeOfCharacter(from: allowedCharacters.inverted) == nil {
        textField.text = textField.text!.replacingOccurrences(of: "$", with: "") + string + "$"
      }
      return false
    default:
      break
    }
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    switch textField {
    case carNameTextField:
      priceTextField.becomeFirstResponder()
    case priceTextField:
      engineTextField.becomeFirstResponder()
    default:
      break
    }
    return true
  }
  
}

extension AddCarViewController: UIPickerViewDelegate, UIPickerViewDataSource {
  // MARK: - UIPickerView delegate and datasource methods
  
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return pickerDataSource.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return pickerDataSource[row]
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    (view.currentFirstResponder() as? UITextField)?.text = pickerDataSource[row]
  }
  
}
