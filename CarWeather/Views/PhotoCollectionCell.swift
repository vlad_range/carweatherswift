//
//  PhotoCollectionCell.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 25/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

final class PhotoCollectionCell: UICollectionViewCell {
  // MARK: - IBOutlets
  
  @IBOutlet weak var imageView: UIImageView!
  
  // MARK: - Properties
  
  var path: String? {
    didSet {
      if let path = path {
        imageView.loadImageFromLocal(path: path)
      }
    }
  }
  
  // MARK: - UICollectionView life cycle
  
  override func prepareForReuse() {
    super.prepareForReuse()
    imageView.stopLoading()
    imageView.image = nil
  }
}
