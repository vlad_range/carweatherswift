//
//  AddPhotoView.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 25/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

protocol AddPhotoViewDelegate: class {
  /// Handles add photo button tapping
  func photosViewDidTappedAddPhotos()
}

final class AddPhotoView: UIView {
  // MARK: - Constants
  
  struct cellIdentifiers {
    static let photoCell = "photoCell"
    static let addPhotoCell = "addPhotoCell"
  }
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  // MARK: - Propeerties
  
  weak var delegate: AddPhotoViewDelegate?
  
  var photoPaths: [String] = [] {
    didSet {
      collectionView.reloadData()
    }
  }
  
}

extension AddPhotoView: UICollectionViewDelegate, UICollectionViewDataSource {
  // MARK: - UICollectionView delegate and datasource methods
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return photoPaths.count + 1
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if indexPath.item == photoPaths.count {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifiers.addPhotoCell, for: indexPath) as? AddPhotoCollectionCell
      return cell ?? UICollectionViewCell()
    }
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifiers.photoCell, for: indexPath) as? PhotoCollectionCell
    cell?.path = photoPaths[indexPath.row]
    return cell ?? UICollectionViewCell()
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if indexPath.item == photoPaths.count {
      delegate?.photosViewDidTappedAddPhotos()
    }
  }
  
}
