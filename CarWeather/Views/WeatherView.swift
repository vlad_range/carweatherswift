//
//  WeatherView.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 23/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit
import SDWebImage

final class WeatherView: UIView {
  
  // MARK: - IBOulets
  
  @IBOutlet weak var temperatureLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var cityLabel: UILabel!
  @IBOutlet weak var iconImageView: UIImageView!
  
  // MARK: - Properties
  
  var weather: Weather? {
    didSet {
      if let weather = weather {
        temperatureLabel.text = "\((weather.temperature) > 0 ? "+" : "")\(weather.temperature)"
        descriptionLabel.text = weather.weatherDescription
        cityLabel.text = weather.cityName
        iconImageView.sd_setImage(with: URL(string: weather.iconPath!), completed: nil)
      }
    }
  }
}
