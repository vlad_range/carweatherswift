//
//  CarTableViewCell.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 23/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

final class CarTableViewCell: UITableViewCell {
  // MARK: - IBOutlets

  @IBOutlet weak var carImageView: UIImageView!
  @IBOutlet weak var carLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  
  // MARK: - Properties
  
  public var car: Car? {
    didSet {
      carLabel.text = car?.name
      priceLabel.text = "\(car?.price ?? 0)$"
      carImageView.loadImageFromLocal(path: car?.imagePaths?.first ?? "")
    }
  }
  
  // MARK: - UITableViewCell life cycle
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
    carLabel.text = ""
    priceLabel.text = ""
    carImageView.stopLoading()
    carImageView.image = nil
  }

}
