//
//  PhotoView.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 25/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

final class PhotosView: UIView {
  
  // MARK: - Constants
  static let photoCellIdentifier = "photoCell"
  
  // MARK: - IBOutlets
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var pageControl: UIPageControl!
  
  // MARK: - Properties

  var imagePaths: [String] = [] {
    didSet {
      collectionView.reloadData()
      pageControl.numberOfPages = imagePaths.count
    }
  }
  
  // MARK: - IBActions
  
  @IBAction func pageChanged(_ sender: UIPageControl) {
    let pageWidth = frame.width
    
    collectionView.setContentOffset(CGPoint(x: CGFloat(pageWidth * CGFloat(sender.currentPage)),
                                            y: collectionView.contentOffset.y),
                                    animated: true)
  }

}

extension PhotosView: UICollectionViewDataSource {
  // MARK: - Collection View Data Source
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return imagePaths.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotosView.photoCellIdentifier, for: indexPath) as? PhotoCollectionCell {
      cell.path = imagePaths[indexPath.row]
      return cell
    }
    return UICollectionViewCell()
  }
  
}

extension PhotosView: UICollectionViewDelegate {
  // MARK: - Collection View Delegate
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let pageWidth = frame.width
    let currentPageNumber: Int = Int(scrollView.contentOffset.x / pageWidth)
    pageControl.currentPage = currentPageNumber
  }
  
}

extension PhotosView: UICollectionViewDelegateFlowLayout {
  // MARK: - UICollectionView Flow Layout customization
  
  public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return collectionView.frame.size
  }
  
}

