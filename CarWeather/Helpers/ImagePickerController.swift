//
//  ImagePickerController.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 25/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

public typealias ImagePickerControllerDelegate = UIImagePickerControllerDelegate & UINavigationControllerDelegate

/// Implements <code>UIImagePickerController</code> with alert actions for choosing image sources and provides properties to customize text for them
public class ImagePickerController : UIImagePickerController {
  
  // MARK: - Structs
  
  /**
   Image source options for image picking
   
   - camera: Camera option
   - photoLibrary: Photo library option
   - savedPhotosAlbum: Last saved photo option
   */
  public struct ImageSourceOptions : OptionSet {
    // MARK: - Options
    
    ///Camera option
    public static let camera = ImageSourceOptions(rawValue:1)
    ///Photo library option
    public static let photoLibrary  = ImageSourceOptions(rawValue:2)
    /// Last saved photo option
    public static let savedPhotosAlbum = ImageSourceOptions(rawValue:4)
    
    // MARK: RawValue initialization
    /// :nodoc:
    public let rawValue : Int
    /// :nodoc:
    public init(rawValue:Int){ self.rawValue = rawValue}
  }
  
  // MARK: - Properties
  
  /// Text for take photo action
  public static var takePhotoText: String = "Take Photo"
  /// Text for choose photo action
  public static var choosePhotoText: String = "Choose Photo"
  /// Text for choose saved photo action
  public static var chooseSavedPhotoText: String = "Choose Saved Photo"
  /// Text for take photo action
  public static var cancelText: String = "Cancel"
  /// Tint color for buttons
  public var actionSheetTintColor: UIColor = .black
  
  // MARK: - Methods
  
  /**
   Presents alert view with image sources in target view controller
   
   - parameters:
   - viewController: UIViewConroller on which alert will be presented
   - imageSourceOptions: Image source options to show in alert view
   */
  public func presentActionSheet(for viewController: UIViewController,
                                 imageSourceOptions: ImageSourceOptions = [ImageSourceOptions.camera,
                                                                           ImageSourceOptions.savedPhotosAlbum]) {
    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    alertController.view.tintColor = actionSheetTintColor
    
    weak var weakSelf = self
    weak var weakViewController = viewController
    
    if imageSourceOptions.contains(.camera) {
      let takePhotoAction = UIAlertAction(title: ImagePickerController.takePhotoText, style: .default) { (action) in
        alertController.dismiss(animated: true)
        weakSelf?.sourceType = .camera;
        weakViewController?.present(weakSelf!, animated: true, completion: nil)
      }
      alertController.addAction(takePhotoAction)
    }
    
    if imageSourceOptions.contains(.photoLibrary) {
      let choosePhotoAction = UIAlertAction(title: ImagePickerController.choosePhotoText, style: .default) { (action) in
        alertController.dismiss(animated: true)
        weakSelf?.sourceType = .photoLibrary;
        weakViewController?.present(weakSelf!, animated: true, completion: nil)
      }
      alertController.addAction(choosePhotoAction)
    }
    
    if imageSourceOptions.contains(.savedPhotosAlbum) {
      let chooseSavedPhotoAction = UIAlertAction(title: ImagePickerController.chooseSavedPhotoText, style: .default) { (action) in
        alertController.dismiss(animated: true)
        weakSelf?.sourceType = .savedPhotosAlbum;
        weakViewController?.present(weakSelf!, animated: true, completion: nil)
      }
      alertController.addAction(chooseSavedPhotoAction)
    }
    
    let cancelAction = UIAlertAction(title: ImagePickerController.cancelText, style: .cancel) { (action) in
      alertController.dismiss(animated: true, completion: nil)
    }
    
    alertController.addAction(cancelAction)
    
    viewController.present(alertController, animated: true, completion: nil)
  }
  
}

