//
//  AppDelegate.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 22/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit
import MagicalRecord

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    CoreDataManager.initContext()
    return true
  }

}

