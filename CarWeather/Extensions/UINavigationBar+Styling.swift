//
//  UINavigationBar+Styling.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 24/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

extension UINavigationBar {
  
  // MARK: - Adds bottom line for navigationBar
  
  func setBottomBorderColor(color: UIColor, height: CGFloat) {
    let bottomBorderRect = CGRect(x: 0, y: frame.height, width: frame.width, height: height)
    let bottomBorderView = UIView(frame: bottomBorderRect)
    bottomBorderView.backgroundColor = color
    addSubview(bottomBorderView)
  }
  
}
