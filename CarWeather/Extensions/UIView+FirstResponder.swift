//
//  UIView+FirstResponder.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 24/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

extension UIView {
  
  // MARK: - Helper for finding current first responder
  
  /// Return current first responder
  public func currentFirstResponder() -> UIResponder? {
    if self.isFirstResponder {
      return self
    }
    
    for view in self.subviews {
      if let responder = view.currentFirstResponder() {
        return responder
      }
    }
    
    return nil
  }
  
}
