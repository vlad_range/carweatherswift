//
//  UIImageView+ImageLoader.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 25/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

extension UIImageView {
  
  // MARK: - Helpers for loading image from local storage
  
  func loadImageFromLocal(path: String) {
    performSelector(inBackground: #selector(load(path:)), with: path)
  }
  
  func stopLoading() {
    NSObject.cancelPreviousPerformRequests(withTarget: self)
  }
  
  @objc private func load(path: String) {
    let fullPath = "\(FileManager.documentsPath)/\(path)"
    do {
      let data = try Data(contentsOf: URL(fileURLWithPath: fullPath))
      var image = UIImage(data: data)
      image = image?.preloadedImage()
      performSelector(onMainThread: #selector(set(image:)), with: image, waitUntilDone: false)
    } catch {
      print("Error: \(error)")
    }
  }
  
  @objc private func set(image: UIImage) {
    self.image = image
  }
  
}
