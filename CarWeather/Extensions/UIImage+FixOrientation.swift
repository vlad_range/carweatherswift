//
//  UIImage+FixOrientation.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 25/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

extension UIImage {
  
  // MARK: - UIImage helpers
  
  /// Fixed orientaion for images taken from camera
  /// - returns: Fixed Image
  func fixOrientation() -> UIImage {
    if (imageOrientation == .up) {
      return self
    }
    
    UIGraphicsBeginImageContextWithOptions(size, false, scale)
    let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    draw(in: rect)
    
    let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    
    return normalizedImage
  }
  
  /// Decompress and preload UIImage for fix stutter
  /// - returns: Preloaded image
  func preloadedImage() -> UIImage {
    
    // make a bitmap context of a suitable size to draw to, forcing decode
    let width = cgImage!.width
    let height = cgImage!.height
    
    let colourSpace = CGColorSpaceCreateDeviceRGB()
    
    let imageContext = CGContext.init(data: nil,
                                      width: width,
                                      height: height,
                                      bitsPerComponent: 8,
                                      bytesPerRow: width * 4,
                                      space: colourSpace,
                                      bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue |    CGBitmapInfo.byteOrder32Little.rawValue)
    
    // draw the image to the context, release it
    imageContext?.draw(cgImage!, in: CGRect(x: 0, y: 0, width: width, height: height))
    
    // now get an image ref from the context
    if let outputImage = imageContext?.makeImage() {
      let cachedImage = UIImage(cgImage: outputImage)
      return cachedImage
    }
    
    print("Failed to preload the image")
    return self
  }
}
