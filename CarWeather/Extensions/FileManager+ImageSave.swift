//
//  FileManager+ImageSave.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 25/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import Foundation
import UIKit

extension FileManager {
  // MARK: - Helpers for work with files
  
  static var documentsPath: String {
    return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
  }
  
  static func saveToDisk(image: UIImage, with name: String) -> String? {
    let data = UIImagePNGRepresentation(image)!
    do {
      let imagePath = "\(FileManager.documentsPath)/\(name)"
      try data.write(to: URL(fileURLWithPath: imagePath))
      return imagePath
    } catch {
      print("Error")
    }
    return nil
  }
}
