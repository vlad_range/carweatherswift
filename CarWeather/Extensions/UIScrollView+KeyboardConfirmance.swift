//
//  UIScrollView+KeyboardConfirmance.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 24/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit
import Foundation

extension UIScrollView {
  // MARK: - Keyboard notification setup
  
  /**
   Adds observers for keyboard events (didShow, didHide)
   
   - parameter additionalHeight: Height for additional inset scrollView height
   */
  public func setupKeyboardObserving(additionalHeight: CGFloat = 0) {
    NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { [weak self] notification in
      if let weakSelf = self {
        weakSelf.didShowKeyboard(notification, height: additionalHeight)
      }
    }
    
    NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) { [weak self] notification in
      if let weakSelf = self {
        weakSelf.willHideKeyboard(notification)
      }
    }
  }
  
  /// Removes observers
  public func removeKeyboardObservers() {
    NotificationCenter.default.removeObserver(self)
  }
  
  // MARK: - Keyboard Notification Handle
  
  /**
   Handles show keyboard notification and check if current text field is visible. If not visible scroll to corresponding text field
   
   - parameter notification: UIKeyboardDidShow notification
   */
  func didShowKeyboard(_ notification: Notification, height: CGFloat) {
    if let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
      UIView.animate(withDuration: notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval, animations: {
        self.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.size.height + height, right: 0)
      })
      
      var visibleRect = self.frame
      visibleRect.size.height -= keyboardFrame.size.height
      
      
      if let currentTextField = currentFirstResponder() as? UIView, !visibleRect.contains(currentTextField.frame) {
        self.scrollRectToVisible(currentTextField.frame, animated: true)
      }
    }
  }
  
  /**
   Handles hide keyboard notification and return scroll to it's origin offset
   
   - parameter notification: UIKeyboardDidHide notification
   */
  func willHideKeyboard(_ notification: Notification) {
    UIView.animate(withDuration: notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval, animations: {
      self.contentInset = .zero
    })
  }
  
  // MARK: - Touch handling
  
  open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let textField = currentFirstResponder() as? UITextField {
      textField.endEditing(true)
    }
  }
  
}

