//
//  UIViewController+Alert.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 25/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

/// Show alert methods
public extension UIViewController {
  
  // MARK: - Enums
  
  /**
   Buttons for alert view
   
   - yes: Yes button
   - no: No button
   - cancel: Cancel button
   - ok: Ok button
   */
  public enum AlertButtons: String {
    /// Yes button
    case yes = "Yes"
    /// No button
    case no = "No"
    /// Cancel button
    case cancel = "Cancel"
    /// Ok button
    case ok = "Ok"
  }
  
  // MARK: - Alert Methods
  
  /**
   Shows simple UIAlert with text, title and button "OK"
   
   - parameters:
   - message: Message text for alert popup
   - title: Title for alert popup
   - tintColor: Color for action titles
   */
  public func showAlert(message: String? = nil, title: String? = nil, tintColor: UIColor = .black, buttons: AlertButtons..., completion: (() -> Void)? = nil, buttonCompletion: ((_ button: AlertButtons?) -> Void)? = nil) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alertController.view.tintColor = tintColor
    alertController.modalTransitionStyle = .crossDissolve
    alertController.modalPresentationStyle = .overCurrentContext
    var buttons = buttons
    
    if buttons.count == 0 {
      buttons.append(.ok)
    }
    
    buttons.forEach { (button) in
      let action = UIAlertAction(title: button.rawValue, style: .default, handler: { (action) in
        alertController.view.tintColor = tintColor
        buttonCompletion?(AlertButtons(rawValue: action.title ?? ""))
        alertController.dismiss(animated: true, completion: nil)
      })
      alertController.addAction(action)
    }
    
    present(alertController, animated: true, completion: completion)
  }
  
}
