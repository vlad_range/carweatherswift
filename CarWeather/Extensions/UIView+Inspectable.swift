//
//  UIView+Inspectable.swift
//  CarWeather
//
//  Created by Vladislav Perinskiy on 23/11/17.
//  Copyright © 2017 Vladislav Perinskiy. All rights reserved.
//

import UIKit

@IBDesignable class CustomView: UIView {}
@IBDesignable class CustomButton: UIButton {}

/// IBInspectable extension for some CALayer properties in UIView
extension UIView {
  
  // MARK: - Properties
  
  /// Radius of CALayer's corner radius. Used for rounding
  @IBInspectable public var cornerRadius: CGFloat {
    get { return layer.cornerRadius }
    set {
      layer.masksToBounds = true
      layer.cornerRadius = newValue
    }
  }
  
  /// Width of CALayer's border
  @IBInspectable public var borderWidth: CGFloat {
    get { return layer.borderWidth }
    set { layer.borderWidth = newValue }
  }
  
  /// Color of CALayer's border
  @IBInspectable public var borderColor: UIColor? {
    get {
      if let layerBorderColor = layer.borderColor {
        return UIColor(cgColor:layerBorderColor)
      }
      return nil
    }
    
    set { layer.borderColor = newValue?.cgColor }
  }
  
  /// Computed property for shadow color
  @IBInspectable public var shadowColor: UIColor? {
    get {return UIColor(cgColor: layer.shadowColor!)}
    set {
      layer.masksToBounds = false
      layer.shadowColor = newValue?.cgColor
    }
  }
  
  /// Computed property for shadow offset
  @IBInspectable public var shadowOffset: CGSize {
    get {return layer.shadowOffset}
    set {
      layer.masksToBounds = false
      layer.shadowOffset = newValue
    }
  }
  
  /// Computed property for shadow radius
  @IBInspectable public var shadowRadius: CGFloat {
    get {return layer.shadowRadius}
    set {
      layer.masksToBounds = false
      layer.shadowRadius = newValue
    }
  }
  
  /// Computed property for shadow opacity
  @IBInspectable public var shadowOpacity: Float {
    get {return layer.shadowOpacity}
    set {
      layer.masksToBounds = false
      layer.shadowOpacity = newValue
    }
  }
  
}

